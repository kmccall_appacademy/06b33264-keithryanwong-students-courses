class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    unless @courses.include?(course)
      raise if @courses.any? do |enrolled|
        course.conflicts_with?(enrolled)
      end
      
      @courses.push(course)
      course.students.push(self)
    end
  end

  def course_load
    load_per_dept = Hash.new(0)
    @courses.each do |course|
      load_per_dept[course.department] += course.credits
    end
    load_per_dept
  end
end
